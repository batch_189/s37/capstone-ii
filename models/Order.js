const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: true
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    totalAmount: {
        type: Number,
    },
    status: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Order", orderSchema);