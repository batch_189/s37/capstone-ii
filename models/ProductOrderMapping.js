const mongoose = require('mongoose');

const productOrderMapping = new mongoose.Schema({

    orderId: {
        type: String,
        required: true
    },
    productId: {
        type: String,
        required: true
    },
    qty: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    updatedOn: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("ProductOrderMapping", productOrderMapping);