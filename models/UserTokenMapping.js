const mongoose = require('mongoose');

const userTokenMapping = new mongoose.Schema({

    userId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("UserTokenMapping", userTokenMapping);