const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/UserRoutes');
const productRoutes = require('./routes/ProductRoutes');
const orderRoutes = require('./routes/OrderRoutes');

const port = 5000
const app = express();

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// mongoose.connect(`mongodb://127.0.0.1:27017/s37-s41`, {
mongoose.connect(`mongodb+srv://michella:admin123@cluster0.6wklp.mongodb.net/CapstoneII?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now Connected to MongoDB atlas'));

app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
  });
  