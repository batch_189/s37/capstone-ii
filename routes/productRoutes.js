const express = require('express');
const router = express.Router();
const productService = require('../services/ProductService');

const auth = require('../auth');

// Creating A product
router.post('/', auth.verify, (request, response) => {

    let userAdmin = auth.decode(request.headers.authorization); 
     productService.addProduct(request.body, userAdmin, response).then(fromProductService => response.send(fromProductService));
});

// Get Active products
router.get('/', (request, response) => {
    productService.activeProducts().then(fromProductService => response.send(fromProductService));
});

// Get product by ID
router.get('/:productId', (request, response) => {
    productService.getProductById(request.params).then(fromProductService => response.send(fromProductService));
});

// Update Product
router.put('/:productId/', auth.verify, (request, response) => {

    let data = auth.decode(request.headers.authorization).isAdmin
    productService.updateProduct(request.params, request.body, data, response).then(fromProductService => response.send(fromProductService));
});

// Archive Product
router.put('/:productId/archive', auth.verify, (request, response) => {

    let data = auth.decode(request.headers.authorization).isAdmin
    productService.archiveProduct(request.params, request.body, data).then(fromProductService => response.send(fromProductService));
});

module.exports = router;