const express = require('express');
const router = express.Router();
const orderService = require('../services/OrderService');
const auth = require('../auth');
const { response } = require('express');

// create order
router.post('/', auth.verify, (request, response) => {
    let decodedAuth =  auth.decode(request.headers.authorization);

    let userData = {
        userId: decodedAuth.id,
        isAdmin: decodedAuth.isAdmin
    };
    orderService.addOrder(userData, request.body, response)
});


// Get All Orders
router.get('/', auth.verify, (request, response) =>{

    let userData = auth.decode(request.headers.authorization);
    orderService.getAllOrders(userData, response).then(fromOrderService => response.send(fromOrderService));
});

// Get All Orders
router.get('/current', auth.verify, (request, response) =>{

    let userData = auth.decode(request.headers.authorization);
    orderService.getCurrentOrder(userData, response).then(fromOrderService => response.send(fromOrderService));
});

// Get order by ID
router.get('/:orderId', (request, response) => {
    orderService.getOrderById(request.params).then(fromOrderService => response.send(fromOrderService))
});


module.exports = router;