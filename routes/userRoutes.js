const express = require('express');
const router = express.Router();
const userService = require('../services/UserService');
const orderService = require('../services/OrderService');
const auth = require('../auth');


router.post("/checkEmail", (request,response) => {
    userService.isEmailExists(request.body).then(fromUserService => response.send (fromUserService))
});

// Registration
router.post('/', (request, response) => {
    userService.registerUser(request.body).then(fromUserService => response.send(fromUserService));
});


// Login
router.post('/login', (request, response) => {
    userService.loginUser(request.body).then(fromUserService => response.send(fromUserService));
});


// user details
router.get("/details", auth.verify, (request, response) => {
        
    const userData = auth.decode(request.headers.authorization);
    userService.getProfile({id : userData.id}).then(fromUserService => response.send(fromUserService));

});



// Set as Admin
router.put('/:userId/setAsAdmin', auth.verify, (request, response) => {

    let isLoginAdmin = auth.decode(request.headers.authorization).isAdmin;
    if (!isLoginAdmin) {
        response.status("400").send("Token  not admin");
    } else {
        userService.updateUserAdmin(request.params.userId).then(fromUserService => response.send(fromUserService));

    }
});


// Get Users by products
router.get('/products', auth.verify, (request, response) => {

    let userData =  auth.decode(request.headers.authorization);
    orderService.getUsersByProduct(userData, request.body, response);
});


// // User Logout
// router.delete('/logout', auth.verify, (request, response) => {
//     console.log(response);
//     let userId = auth.decode(request.headers.authorization).id;
//     userService.userLogout(userId).then(fromUserService => response.send(fromUserService))
// });

router.delete('/logout', auth.verify, (request, response) => {

    auth.logout(auth.decode(request.headers.authorization).id);
    response.status(201).send("user logged out");
});

module.exports = router;

