const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// Check if email is already exists
module.exports.isEmailExists = (requestBody) => {


	// The result is sent back to the frontend via the "then" method found in the route file
        return User.find({email: requestBody.email}).then(result  => {

            // The "find" method returns a record if a match is found
            return result.length > 0
        });
    };

// Register
module.exports.registerUser = (requestBody) => {

    let newUser = new User ({
        
        email: requestBody.email,
        password: bcrypt.hashSync(requestBody.password, 10)
    });

    return newUser.save().then((user, error) => {
        return error  ? false : {access: auth.createAccessToken(user)}
    });
};


// Login
module.exports.loginUser = (requestBody) => {

    return User.findOne({email: requestBody.email}).then(result => {
        const invalidEmail = {
            message : "Invalid Email",
            code: 403
        }
        if (result == null){
            return invalidEmail;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);
            if (isPasswordCorrect){
                return {access: auth.createAccessToken(result)};
            } else{

                const invalidPassword = {
                    message : "Invalid password",
                    code: 403
                }
                return invalidPassword;
            }
        };
    });
};


// Retrieve user details
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";

		return result;

	});
};



// Update specific field
module.exports.updateUserAdmin = (userId) => {
    
    return User.findById(userId).then((user) => {
        console.log("userservice " + user)
      
       if (user) { 
            user.isAdmin = true;
            user.save();
            return "Update Successful!"
        } else {
            return 'Failed to update'
        }
    });
};


// User Logout
module.exports.userLogout = (userId, response) => {
    console.log("logout1 " + userId)
    if (userId){
        console.log("logout2 " + userId)
        console.log(response);
        
        response.status(201).send("yay");

    } else {
     response.status(400).send('Unable to logout');
    }
};