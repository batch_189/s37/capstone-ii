const Product = require('../models/Product');
const auth = require('../auth');
const { response } = require('express');

// Creating A product
module.exports.addProduct = async (requestBody, userAdmin, response) => {
    console.log(userAdmin)
    if (userAdmin.isAdmin) {
        let newProduct = new Product({
            name: requestBody.name,
            description: requestBody.description,
            stock: requestBody.stock,
            price: requestBody.price,
            orderId: requestBody.orderId,
            productId: requestBody.productId,
            qty: requestBody.qty,
        });
        newProduct.save();
        return await "Product Created Successfully!"
    } else {
        response.status(401).send("Admin Only!");
    }
};

// Get Active products
module.exports.activeProducts = () => {

    return Product.find({isActive: true}).then(result => {
        return result
    });
};

// Get specific product by ID
module.exports.getProductById = (productById) => {
    console.log(productById)
    return Product.findById(productById.productId).then(result => {
        return result
    });

};

// Update Product by ID
module.exports.updateProduct = (productToBeUpdate ,requestBody, data, response) => {

    console.log(data);
    if (data){

        let updatedProduct = {
            name: requestBody.name,
            description: requestBody.description,
            price: requestBody.price
        };
        return Product.findByIdAndUpdate(productToBeUpdate.productId, updatedProduct).then((result, error) => {
            console.log(result)
            return result
        });
    } else{
        response.status(401).send("Admin Only!")
    }
};

// Product Archive
module.exports.archiveProduct = async(productToBeArchive, requestBody, data) => {

    if (data){
        let updatedArchive = {isActive: requestBody.isActive};
 
        return await Product.findByIdAndUpdate(productToBeArchive.productId, updatedArchive).then((result, error) => {
            return error ? false : true
        });
    } else {
        return "Admin Only"
    }
       
};



