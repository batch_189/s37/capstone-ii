const Order = require('../models/Order');
const ProductOrderMapping = require('../models/ProductOrderMapping');
const Product = require('../models/Product');
const User = require('../models/User');

const statusAwaitingCheckout = "Awaiting Checkout";
const statusAwaitingPayment = "Awaiting Payment";
const statusPaymentSuccess = "Payment Success";

// Order
module.exports.addOrder = async (userData, requestBody, response) => {

    if (userData.isAdmin === true){
        return "You are not allowed to order"

    } else {
        let order = await Order.findOne({status: statusAwaitingCheckout, userId: userData.userId});
        if (!order) {
            order = new Order();
            order.userId = userData.userId;
            order.status = statusAwaitingCheckout;
            order.save();
            console.log("new order created for user: " + userData.userId);
         };

         console.log("order: " + order.id)
         console.log("productId: " + requestBody.productId)
        let product = await Product.findOne({_id: requestBody.productId});
        if (!product) {
            console.log("d nmn nag eexist");
            response.send("invalid product id");
            return;
        }

        let productOrderMapping = new ProductOrderMapping();
            productOrderMapping.orderId = order.id;
            productOrderMapping.productId = requestBody.productId;
            productOrderMapping.qty = requestBody.qty;
            productOrderMapping.price = product.price;
           
        productOrderMapping.save();
        response.send(productOrderMapping);
        // console.log(productOrderMapping)
    };
}; 


// Get All Orders
module.exports.getAllOrders = (userData, response) => {
    console.log(userData)

    if(userData.isAdmin){
        return Order.find({}).then(result => {
            console.log(userData.isAdmin)
            console.log(result)
            return result
        });
    } else {
        response.status(401).send("Admin Only!")
    }
};


module.exports.getCurrentOrder = async (userData, response) => {
    console.log(userData)

    //step 1 get cart with status awaiting checkout
    var cartAwaitingCheckout = await Order.findOne({status: statusAwaitingCheckout})
                .then (order => {
                    return order;
                });

    if (cartAwaitingCheckout == null) {
        return "";
    }

    console.log(cartAwaitingCheckout);
    // step 2 get all products on the retrieved cart
    var orderProducts = await ProductOrderMapping.find({orderId: cartAwaitingCheckout.id})
                   .then(orderProducts => {
                    console.log(cartAwaitingCheckout.id + "here at orderservice")
                   return orderProducts;
               });


    // step 3 get all product details e.g name ad price
    var products = await getProducts(orderProducts)

    // step 4 calculate cart total amount
    var totalCartAmount = 0;
    for (const p of products) {
        totalCartAmount += p.totalAmount;
    }

    // step 5 build response
    var responseJson = JSON.stringify({
        orderId : cartAwaitingCheckout.id,
        products : products,
        totalCartAmount: totalCartAmount
       });

    
    console.log("responseJson: " + responseJson)
    return responseJson;
};



// Get order by ID
module.exports.getOrderById = (orderId) => {
  
    console.log(orderId)
    return Order.findById(orderId.orderId).then(result => {
        console.log(result)
        return result
    });
};

async function getProducts(orderProducts) {
    var products = [];
    for  (const item of orderProducts) {
        console.log("fetching: " + item.productId)
        await Product.findById(item.productId).then(product => {
        console.log("found product" + product) 
            var productItem = {
                productId : item.productId,
                name : product.name,
                qty  : item.qty,
                price :  item.price,
                totalAmount : item.qty * item.price
            };
            products.push(productItem);
            console.log(productItem)
       });
    }

    return products;
}

// Get Users by products
module.exports.getUsersByProduct = async (userData, requestBody, response) => {

    return await Product.findOne({name: requestBody.name}).then(productResult => {
        console.log("orderService ProductId result \n", productResult);
        
        return ProductOrderMapping.find({productId: productResult.id}).select({_id: 0, orderId: 1}).then(mappingResult => {
            console.log("mappingResult", mappingResult);

            let orderIds = [];
            
            mappingResult.forEach(result => {
                orderIds.push(result.orderId);
            });
            console.log("orderIds \n" + orderIds)

            return Order.find({ _id: { $in: orderIds }}).then(orderResult => {

                let orderUserIds = [];

                orderResult.forEach(order => {
                    orderUserIds.push(order.userId);
                });

                console.log(orderUserIds);
                return User.find({'_id': {$in: orderUserIds}}).then(users => {

                    let userEmails = [];

                    users.forEach(user => {
                        userEmails.push(user.email);
                    });

                    console.log("userEmails: " + userEmails);
                    response.status(200).send(userEmails);
                });
            });
        });
    });

};

