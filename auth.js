const jwt = require('jsonwebtoken');
const UserTokenMapping = require('./models/UserTokenMapping');
const User = require('./models/User');

const secretToken = "zxcvbnm12";

module.exports.createAccessToken = (user) => {

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    let token =  jwt.sign(data, secretToken, {});


   let userTokenMapping = new UserTokenMapping;
        userTokenMapping.userId = user._id;
        userTokenMapping.token = token;
        userTokenMapping.save();
    return token;
};


module.exports.verify = (request, response, next) => {
    
    // console.log(request.headers);
    let token = request.headers.authorization

    if (typeof token !== "undefined"){

        if (!UserTokenMapping.findOne({userId: this.decode(token).id})) {
            console.log("token not  found")
            return response.send({auth: "failed"});
        } else {
        }

        token = token.slice(7, token.length);
        return jwt.verify(token, secretToken, (error, data) => {

            if (error){
                return response.send({auth: "failed"});
            } else {

                next();
            }
        });
    } else {
        return response.status(122)
    }
};

module.exports.decode = (token) => {

    token = token.slice(7, token.length);
    return jwt.verify(token, secretToken, (error, data) => {
        
        if (error){
            return null
        } else{
            return jwt.decode(token, {complete: true}).payload
        }
    });
};

module.exports.logout = (userId) => {
    console.log("auth here " + userId)

    UserTokenMapping.deleteOne({userId: userId}).then(result => {
        console.log("deleted token mapping: " + result + userId);
    });
}